# HelloWorld Java SpringBoot  #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###

* Download this repo
* Install Java 8
* Install Spring
* Install maven

## Build and run
To build, import the pom.xml into your IDE, or open command line and type
```
mvn clean compile package
```
This compiles the code, downloads dependencies, and builds a fat jar in /target.

To run, type
```
java -jar target\FirstSpringApp-0.0.1-SNAPSHOT.jar
```
See the result at http://localhost:8080/greeting?name=YourName

## Docker
The Dockerfile is aimed at a RaspberryPi architecture. Change the first line to
match your architecture. See https://hub.docker.com/.

When you have docker installed on the remote machine, logon to the remote machine (by SSH) and type: 

```
docker build -t spring/hello-world https://rschellius@bitbucket.org/AEI-informatica/helloworld.git
docker run -p 8083:8080 -d spring/hello-world	// run as deamon on external port 8081
docker run --restart=unless-stopped -p 8083:8080 -d spring/hello-world
docker ps -a 								// shows the container ID
docker exec -it <container ID> /bin/bash	// connects a shell
```

See a running example on http://home.level-ict.nl:8083/greeting.
See also http://home.level-ict.nl:8083/greeting?name=John.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact